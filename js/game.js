// Fonction jQuery : exécute le jeu une fois que le DOM est chargé
$(function() {

var Settings = {
		pseudo
	};

	// On attend que l'utilisateur clique sur le bouton "submit" pour lancer le jeu : il valide le formulaire
	$("form").on("submit", function (e) {
    	e.preventDefault(); // On ne recharge pas la page avec le formulaire
// On récupère le pseudo rentré par l'utilisateur
    	Settings.pseudo = $('input[type=text]#pseudo').val();

    	// On lance le jeu
		initial(Settings);
		click(Settings);
	});
	/* ---------- ---------- */
	/* ----- SETTINGS ------ */
	/* ---------- ---------- */

	// Objet littéral qui stocke l'argent, les vies et la vitesse du jeu
	var	Player = {
			money: 50,
			life : 3,
			speed: 5, // 10 = fast; 50 = normal mode
			time : 5, // time (in sec) before monsters move
			level: 1,
		}; 

	/* ---------- ---------- */
	/* ------ PARCOURS ----- */
	/* ---------- ---------- */

	// Objet littéral qui stocke le parcours des monstres
	var	Parcours = {
			start: 600, 
			sizeCourse: 150,
			course: [
				['down' ,200],
				['left' ,400],
				['down' ,600],
			]
		};

	// On appelle la fonction qui crée le parcours (visuel)
	makeCourse(Parcours);

	/* ---------- ---------- */
	/* ------ TOWERS ------- */
	/* ---------- ---------- */

	var towers = [];  // Tableau qui stocke toutes les tours du jeu

	// On affiche les tours que l'on peut créer à l'écran
	displayTowers(Player, towers); 

	// On appelle la fonction qui permet de créer des tours
	makeTowers(towers, Player);

	/* ---------- ---------- */
	/* ----- MONSTERS ------ */
	/* ---------- ---------- */

	var	monsters = []; // Tableau qui stocke tous les monstres du jeu

	/* ---------- ---------- */
	/* ------- GAME -------- */
	/* ---------- ---------- */

	function initial(gameSettings) {
// On affiche le pseudo de l'utilisateur dans le HTML
	$('h1 span.pseudo').text(gameSettings.pseudo);
	// On masque le div contenant les informations du formulaire
	$('div.infos1').fadeOut();
	// On affiche le jeu
		$('div.game').fadeIn();
		// On appelle la fonction qui lance le jeu
	startGame(Player, Parcours, monsters, towers);
}


	
})

// ------------------------------------------------------------------------- //
// ----------------------- ALL FUNCTIONS FOR THE GAME ---------------------- //
// ------------------------------------------------------------------------- //




// ----------------------
// --- FUNCTIONS GAME ---
// ----------------------

// Fonction qui déclare les monstres à créer et les stocke dans le tableau des monstres
function makeMonsters(monsters, Parcours) {
	var MonsterToCreate;

	// On crée l'ensemble des monstres que l'on stocke dans un tableau
	for (var i = 0, max = 2; i < max; i++) {
		// On crée un monstre
		MonsterToCreate = new Monster(-100*(i+1), Parcours.start, (i+1)*100, 'Soldat', 20, 'resources/Soldat-1.png');
		monsters.push(MonsterToCreate);
	}

	for (var i = 0, max = 2; i < max; i++) {
		// On crée un monstre
		MonsterToCreate = new Monster(-400*(i+1), Parcours.start, (i+1)*150, 'camion', 30, 'resources/camion-1.png');
		monsters.push(MonsterToCreate);
	}

	for (var i = 0, max = 1; i < max; i++) {
		// On crée un monstre
		MonsterToCreate = new Monster(-200*(i+1), Parcours.start, (i+1)*150, 'tank', 30, 'resources/Tank-2.png');
		monsters.push(MonsterToCreate);
	}

}

// Fonction qui lance le jeu
function startGame(Player, Parcours, monsters, towers) {
	// On affiche les informations du joueur (html)
	makeMonsters(monsters, Parcours);
	console.log('monstre');
	$('.infos span.time').text(Player.time);
	$('.infos span.life').text(Player.life);
	$('.infos span.money').text(Player.money);
	$('.infos span.level').text(Player.level);

	// On lance le décompte
	var timer = setInterval(function() {
		$('.infos span.time').text(Player.time); // On change chaque seconde le temps restant
		if (Player.time <= 0) {

			// On arrête le décompte
			clearInterval(timer);

			// On lance le timer pour déplacer les monstres et attaquer
			monsterMove(Player, Parcours, monsters, towers, Player.speed);
		}
		else {
			Player.time--;
		}
	}, 1000);
}

// ----------------------
// -- FUNCTIONS OTHERS --
// ----------------------

// Fonction qui calcule l'hypotenuse
function calcHypotenuse(a, b) {
  return(Math.sqrt((a * a) + (b * b)));
}

// Fonction qui retourne une valeur comprise en % d'un chiffre
function hpPourcent (hp, hpMax) {
	return parseInt(hp * 100 / hpMax);
}
function click(gameSettings) {

	// Quand l'utilisateur clique sur le carré
	$('.click').click(function(e) {

	

		// On relance le jeu
		initial(gameSettings);
	});
}



